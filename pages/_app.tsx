import { ChakraProvider } from '@chakra-ui/react';
import type { AppProps } from 'next/app';
import '@fontsource/sorts-mill-goudy';
import '@fontsource/unifrakturcook';

import theme from '../src/theme';

const MyApp = ({ Component, pageProps }: AppProps) => (
  <ChakraProvider theme={theme}>
    <Component {...pageProps} />
  </ChakraProvider>
);

export default MyApp;
