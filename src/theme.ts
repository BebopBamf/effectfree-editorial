import { extendTheme } from '@chakra-ui/react';

const fonts = {
  heading: 'Sorts Mill Goudy, serif',
  body: 'Sorts Mill Goudy, serif',
};

const config = {
  initialColorMode: 'system',
  useSystemColorMode: true,
};

const theme = extendTheme({ fonts, config });

export default theme;
