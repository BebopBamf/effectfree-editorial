import {
  Box,
  Container,
  Center,
  Text,
  useColorModeValue,
  useBreakpointValue,
} from '@chakra-ui/react';
import Logo from './logo';

const Navbar = () => {
  const isDesktop = useBreakpointValue({ base: false, lg: true });

  return (
    <Box as="nav" boxShadow={useColorModeValue('sm', 'sm-dark')}>
      <Container>
        {isDesktop && <Text>Test</Text>}
        <Center p="4">
          <Logo />
        </Center>
      </Container>
    </Box>
  );
};

export default Navbar;
