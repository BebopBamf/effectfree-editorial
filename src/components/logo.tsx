import { Heading } from '@chakra-ui/react';

const Logo = () => (
  <Heading fontFamily="UnifrakturCook, cursive">
    The Effect Free Editorial
  </Heading>
);

export default Logo;
